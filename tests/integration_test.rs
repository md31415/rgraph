#![allow(non_snake_case)]

extern crate rgraph;


#[cfg(test)]
mod tests {
    use std::collections::HashSet;
    use rgraph::SGraph;
    use rgraph::path;
    use rgraph::cycle;
    use rgraph::diamant;
    #[test]
    fn test_addvertex() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        assert_eq!(graph.order(), 4usize);
        graph.insertVertex(4);
        assert_eq!(graph.order(), 4usize);
        assert_eq!(graph.size(), 0usize);
    }

    #[test]
    fn test_removevertex() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        graph.deleteVertex(2);
        assert_eq!(graph.order(), 3usize);
        graph.insertVertex(2);
        assert_eq!(graph.order(), 4usize);
        graph.deleteVertex(5);
        graph.deleteVertex(2);
        graph.deleteVertex(2);
        assert_eq!(graph.order(), 3usize);
    }

    #[test]
    fn test_deleteVertexSet() {
        let mut graph: SGraph = diamant();
        let X: HashSet<u32> = [3,4].iter().cloned().collect();
        graph.deleteVertexSet(&X);
        assert_eq!(graph.order(), 2usize);
        assert_eq!(graph.size(), 1usize);
    }

    #[test]
    fn test_addedge() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        assert_eq!(graph.order(), 4usize);
        graph.insertEdge(1,2);
        graph.insertEdge(1,3);
        graph.insertEdge(2,3);
        graph.insertEdge(4,2);
        graph.insertEdge(3,4);
        assert_eq!(graph.size(), 5usize);
        graph.insertEdge(3,4);
        assert_eq!(graph.size(), 5usize);
    }

    #[test]
    fn test_removeedge() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        assert_eq!(graph.order(), 4usize);
        graph.insertEdge(1,2);
        graph.insertEdge(1,3);
        graph.insertEdge(2,3);
        graph.insertEdge(4,2);
        graph.insertEdge(3,4);
        assert_eq!(graph.size(), 5usize);
        graph.deleteEdge(3,4);
        assert_eq!(graph.size(), 4usize);
        graph.deleteEdge(5,6);
        assert_eq!(graph.size(), 4usize);
    }

    #[test]
    fn test_deleteEdgeSet() {
        let mut graph: SGraph = diamant();
        let F: HashSet<(u32,u32)> = [(3,4),(1,2)].iter().cloned().collect();
        graph.deleteEdgeSet(&F);
        assert_eq!(graph.size(), 3usize);
    }

    #[test]
    fn test_removeNeighborEdges() {
        let mut graph: SGraph = diamant();
        graph.removeNeighborEdges(1);
        assert_eq!(graph.size(), 4usize);
    }

    #[test]
    fn test_hasedge() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        assert_eq!(graph.order(), 4usize);
        graph.insertEdge(1,2);
        graph.insertEdge(1,3);
        graph.insertEdge(2,3);
        graph.insertEdge(4,2);
        graph.insertEdge(3,4);
        assert_eq!(graph.hasEdge(1,2), true);
        assert_eq!(graph.hasEdge(3,4), true);
        assert_eq!(graph.hasEdge(1,4), false);
        assert_eq!(graph.hasEdge(1,5), false);
    }

    #[test]
    fn test_degree() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        assert_eq!(graph.order(), 4usize);
        assert_eq!(graph.degree(1), 0usize);
        graph.insertEdge(1,2);
        graph.insertEdge(1,3);
        graph.insertEdge(2,3);
        graph.insertEdge(4,2);
        graph.insertEdge(3,4);
        assert_eq!(graph.degree(1), 2usize);
        assert_eq!(graph.degree(2), 3usize);
        assert_eq!(graph.degree(3), 3usize);
        assert_eq!(graph.degree(4), 2usize);
        assert_eq!(graph.degree(5), 0usize);
    }

    #[test]
    fn test_minmaxdegree() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        assert_eq!(graph.get_min_degree(), 0usize);
        assert_eq!(graph.get_max_degree(), 0usize);
        graph.insertEdge(1,2);
        graph.insertEdge(1,3);
        graph.insertEdge(2,3);
        graph.insertEdge(4,2);
        graph.insertEdge(3,4);
        assert_eq!(graph.get_min_degree(), 2usize);
        assert_eq!(graph.get_max_degree(), 3usize);
    }

    #[test]
    fn test_numiso() {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        graph.insertVertex(4);
        assert_eq!(graph.get_num_iso(), 4usize);
        graph.insertEdge(1,2);
        assert_eq!(graph.get_num_iso(), 2usize);
        graph.insertEdge(1,3);
        assert_eq!(graph.get_num_iso(), 1usize);
        graph.insertEdge(2,3);
        graph.insertEdge(4,2);
        graph.insertEdge(3,4);
        assert_eq!(graph.get_num_iso(), 0usize);
    }

    #[test]
    fn test_Delta() {
        let graph: SGraph = diamant();
        let mut W = HashSet::new();
        W.insert(1);
        assert_eq!(graph.Delta(&W).len(), 0usize);
        W.insert(2);
        assert_eq!(graph.Delta(&W).len(), 1usize);
        W.insert(3);
        assert_eq!(graph.Delta(&W).len(), 3usize);
        let W: HashSet<u32> = [1,4].iter().cloned().collect();
        assert_eq!(graph.Delta(&W).len(), 0usize);
    }

    #[test]
    fn test_delta() {
        let graph: SGraph = diamant();
        let mut W = HashSet::new();
        W.insert(1);
        assert_eq!(graph.delta(&W).len(), 2usize);
        W.insert(2);
        assert_eq!(graph.delta(&W).len(), 3usize);
        W.insert(3);
        assert_eq!(graph.delta(&W).len(), 2usize);
        let W: HashSet<u32> = [1,4].iter().cloned().collect();
        assert_eq!(graph.delta(&W).len(), 4usize);
    }

    #[test]
    fn test_is_clique() {
        let graph: SGraph = diamant();
        let mut W = HashSet::new();
        W.insert(1);
        assert_eq!(graph.is_clique(&W), true);
        W.insert(2);
        assert_eq!(graph.is_clique(&W), true);
        W.insert(3);
        assert_eq!(graph.is_clique(&W), true);
        let W: HashSet<u32> = [1,4].iter().cloned().collect();
        assert_eq!(graph.is_clique(&W), false);
    }

    #[test]
    fn test_product() {
        let graph: SGraph = path(4);
        let graph2: SGraph = path(4);
        let prod_graph = graph.product(&graph2);
        assert_eq!(prod_graph.order(), 16usize);
        assert_eq!(prod_graph.size(), 24usize);
        assert_eq!(prod_graph.get_min_degree(), 2usize);
        assert_eq!(prod_graph.get_max_degree(), 4usize);

        let graph: SGraph = path(4);
        let graph2: SGraph = cycle(5);
        let prod_graph = graph.product(&graph2);
        assert_eq!(prod_graph.order(), 20usize);
        assert_eq!(prod_graph.size(), 35usize);
        assert_eq!(prod_graph.get_min_degree(), 3usize);
        assert_eq!(prod_graph.get_max_degree(), 4usize);
    }

    #[test]
    fn test_tensorProduct() {
        let graph: SGraph = path(4);
        let graph2: SGraph = path(4);
        let prod_graph = graph.tensorProduct(&graph2);
        assert_eq!(prod_graph.order(), 16usize);
        assert_eq!(prod_graph.size(), 18usize);
        assert_eq!(prod_graph.get_min_degree(), 1usize);
        assert_eq!(prod_graph.get_max_degree(), 4usize);

        let graph: SGraph = path(4);
        let graph2: SGraph = cycle(5);
        let prod_graph = graph.tensorProduct(&graph2);
        assert_eq!(prod_graph.order(), 20usize);
        assert_eq!(prod_graph.size(), 30usize);
        assert_eq!(prod_graph.get_min_degree(), 2usize);
        assert_eq!(prod_graph.get_max_degree(), 4usize);
    }

    #[test]
    fn test_lexicographicalProduct() {
        let graph: SGraph = path(4);
        let graph2: SGraph = path(4);
        let prod_graph = graph.lexicographicalProduct(&graph2);
        assert_eq!(prod_graph.order(), 16usize);
        assert_eq!(prod_graph.size(), 60usize);
        assert_eq!(prod_graph.get_min_degree(), 5usize);
        assert_eq!(prod_graph.get_max_degree(), 10usize);

        let graph: SGraph = path(4);
        let graph2: SGraph = cycle(5);
        let prod_graph = graph.lexicographicalProduct(&graph2);
        assert_eq!(prod_graph.order(), 20usize);
        assert_eq!(prod_graph.size(), 95usize);
        assert_eq!(prod_graph.get_min_degree(), 7usize);
        assert_eq!(prod_graph.get_max_degree(), 12usize);
    }

    #[test]
    fn test_strongProduct() {
        let graph: SGraph = path(4);
        let graph2: SGraph = path(4);
        let prod_graph = graph.strongProduct(&graph2);
        assert_eq!(prod_graph.order(), 16usize);
        assert_eq!(prod_graph.size(), 42usize);
        assert_eq!(prod_graph.get_min_degree(), 3usize);
        assert_eq!(prod_graph.get_max_degree(), 8usize);

        let graph: SGraph = path(4);
        let graph2: SGraph = cycle(5);
        let prod_graph = graph.strongProduct(&graph2);
        assert_eq!(prod_graph.order(), 20usize);
        assert_eq!(prod_graph.size(), 65usize);
        assert_eq!(prod_graph.get_min_degree(), 5usize);
        assert_eq!(prod_graph.get_max_degree(), 8usize);
    }

    #[test]
    fn test_join() {
        let graph: SGraph = path(4);
        let graph2: SGraph = path(4);
        let prod_graph = graph.join(&graph2);
        assert_eq!(prod_graph.order(), 8usize);
        assert_eq!(prod_graph.size(), 22usize);
        assert_eq!(prod_graph.get_min_degree(), 5usize);
        assert_eq!(prod_graph.get_max_degree(), 6usize);

        let graph: SGraph = path(4);
        let graph2: SGraph = cycle(5);
        let prod_graph = graph.join(&graph2);
        assert_eq!(prod_graph.order(), 9usize);
        assert_eq!(prod_graph.size(), 28usize);
        assert_eq!(prod_graph.get_min_degree(), 6usize);
        assert_eq!(prod_graph.get_max_degree(), 7usize);
    }
}
