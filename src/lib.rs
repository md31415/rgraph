#![allow(non_snake_case)]

// Class for simple undirected graphs
//
// Copyright (c) 2017 Markus Dod
//
// This file is part of rgraph.
//
// PyGraphEdit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PyGraphEdit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with PyGraphEdit. If not, see <http://www.gnu.org/licenses/>.
//
// @author: Markus Dod
// @license: GPL (see LICENSE.txt)

#[macro_use] extern crate itertools;

use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;

mod consts;

#[derive(Debug)]
pub struct SGraph {
    //vertices: HashSet<u32>,
    adj: HashMap<u32, HashSet<u32>>,
    name: String,
    loops: HashMap<u32, u32>,
    graphtype: consts::graphType
}

impl fmt::Display for SGraph {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Vertices \nEdges {:?}", self.adj)
    }
}

#[allow(dead_code)]
impl SGraph {
    pub fn new() -> SGraph {
        SGraph {adj:HashMap::new(), name:"".to_string(),
                loops:HashMap::new(), graphtype:consts::graphType::None}
    }

    pub fn getVertices(&self) -> HashSet<u32> {
        let mut V = HashSet::new();
        for (v, _) in &self.adj {
            V.insert(v.clone());
        }
        V
    }

    pub fn getV(&self) -> HashSet<u32> {
        self.getVertices()
    }

    pub fn getEdgeSet(&self) -> HashSet<(u32,u32)> {
        let mut E: HashSet<(u32,u32)> = HashSet::new();
        for (u, F) in &self.adj {
            for v in F {
                E.insert((*u,*v));
            }
        }
        E
    }

    pub fn getAdja(&self) -> HashMap<u32, HashSet<u32>> {
        self.adj.clone()
    }

    /// Add a named vertex
    pub fn insertVertex(&mut self, v: u32) {
        if !self.adj.contains_key(&v) {
            self.adj.insert(v.clone(), HashSet::new());
        }
    }

    /// Insert a vertex sets
    pub fn insertVertexSet(&mut self, X: &HashSet<u32>) {
        for v in X {
            self.insertVertex(*v)
        }
    }

    /// Add a new vertex to the graph
    /// returns the vertex
    pub fn newVertex(&mut self) -> u32 {
        let v = self.max_vertex() + 1;
        self.insertVertex(v);
        v
    }

    /// Delete the vertex v in the graph
    pub fn deleteVertex(&mut self, v: u32) {
        self.adj.remove(&v);
        let V = self.getVertices();
        for u in V {
            self.deleteEdge(u,v);
        }
    }

    // Deletes the vertex subset X in the graph
    pub fn deleteVertexSet(&mut self, X: &HashSet<u32>) {
        for v in X {
            self.deleteVertex(*v);
        }
    }

    /// Add an edge (u,v) to the graph
    pub fn insertEdge(&mut self, u: u32, v: u32) {
        if self.adj.contains_key(&u) && self.adj.contains_key(&v) {
            match self.adj.get_mut(&u) {
                Some(n) => n.insert(v),
                _ => false
            };
            match self.adj.get_mut(&v) {
                Some(n) => n.insert(u),
                _ => false
            };
        }
    }

    /// Insert the edges of the set F into the graph
    pub fn insertEdgeSet(&mut self, F: &HashSet<(u32,u32)>) {
        for &(u, v) in F {
            self.insertEdge(u, v);
        }
    }

    /// Remove the edge (u,v) in the graph
    pub fn deleteEdge(&mut self, u: u32, v: u32) {
        match self.adj.get_mut(&u) {
            Some(n) => n.remove(&v),
            _ => false
        };
        match self.adj.get_mut(&v) {
            Some(n) => n.remove(&u),
            _ => false
        };
    }

    /// Deletes the edge set F in the graph
    pub fn deleteEdgeSet(&mut self, F: &HashSet<(u32,u32)>) {
        for &(u, v) in F {
            self.deleteEdge(u, v);
        }
    }

    /// Deletes all incident edges of v
    pub fn removeNeighborEdges(&mut self, v: u32) {
        let N = self.neighbors(v);
        let F = self.Delta(&N);
        for (u, w) in F {
            self.deleteEdge(u, w);
        }
    }

    /// Is the edge (u,v) in the graph?
    pub fn hasEdge(&self, u: u32, v: u32) -> bool {
        match self.adj.get(&u) {
            Some(n) => n.contains(&v),
            _ => false
        }
    }

    /// Contract the edge (u,v) in the graph
    pub fn contractEdge(&mut self, u: u32, v: u32) {
        self.deleteEdge(u, v);
        let neighbors_u = self.neighbors(u);
        for w in neighbors_u {
            self.insertEdge(v, w);
        }
        self.deleteVertex(u);
    }

    /// Aubdivied the edge (u,v) in the graph
    pub fn subdivision(&mut self, u: u32, v: u32) {
        self.deleteEdge(u, v);
        let w = self.newVertex();
        self.insertEdge(u, w);
        self.insertEdge(v, w);
    }

    /// Returns the maximum vertex
    /// # Examples
    ///
    /// ```
    /// use rgraph::SGraph;
    /// let mut graph: SGraph = SGraph::new();
    /// graph.insertVertex(1);
    /// graph.insertVertex(2);
    /// graph.insertVertex(3);
    /// graph.newVertex();
    /// assert_eq!(4u32, graph.max_vertex());
    /// ```
    pub fn max_vertex(&self) -> u32 {
        self.adj.keys().max().unwrap().clone()
    }

    /// Returns the order of the graph
    pub fn order(&self) -> usize {
        self.adj.len()
    }

    /// Returns the size of the graph
    pub fn size(&self) -> usize {
        let mut num_edges: usize = 0;
		for (_, ref neighbors) in self.adj.iter()
		{
			num_edges += neighbors.len();
		}
		num_edges / 2
    }

    /// Returns the degree of the vertex v
    pub fn degree(&self, v: u32) -> usize {
        match self.adj.get(&v) {
            Some(n) => n.len(),
            _ => 0
        }
    }

    /// Get the minimum degree in graph
    pub fn get_min_degree(&self) -> usize {
        let mut mindeg: usize = 10_000_000;
        for (_, a) in &self.adj {
            if a.len() < mindeg {
                mindeg = a.len();
            }
        }
        mindeg
    }

    /// Get the maximum degree in graph
    pub fn get_max_degree(&self) -> usize {
        let mut maxdeg: usize = 0;
        for (_, a) in &self.adj {
            if a.len() > maxdeg {
                maxdeg = a.len();
            }
        }
        maxdeg
    }

    /// Return the number of isolated vertices
    pub fn get_num_iso(&self) -> usize {
        let mut numiso: usize = 0;
        for (_, a) in &self.adj {
            if a.len() == 0 {
                numiso += 1;
            }
        }
        numiso
    }

    /// Returns a hashmap with the degees of the vertices
    pub fn get_degrees(&self) -> HashMap<u32, usize> {
        let mut degs = HashMap::new();
        for (v, a) in &self.adj {
            degs.insert(v.clone(), a.len());
        }
        degs
    }

    /// Returns the open neighborhood of the vertex u
    pub fn neighbors(&self, u: u32) -> HashSet<u32> {
        match self.adj.get(&u) {
            Some(n) => n.clone(),
            _ => HashSet::new()
        }
    }

    /// Returns the neihborhood of the set A
    pub fn neighbors_set(&self, A: &HashSet<u32>) -> HashSet<u32> {
        let mut N = HashSet::new();
        for v in A {
            N = N.union(&self.neighbors(*v)).cloned().collect();
        }
        N.union(&A).cloned().collect()
    }

    /// Returns the closed neighborhood of the set W
    pub fn closed_neighborhood(&self, W: &HashSet<u32>) -> HashSet<u32> {
        let neighbors = self.total_neighborhood(&W);
        neighbors.union(&W).cloned().collect()
    }

    /// Returns the opne neighborhood of the set W
    pub fn open_neighborhood(&self, W: &HashSet<u32>) -> HashSet<u32> {
        let mut neighbors = HashSet::new();
        for w in self.total_neighborhood(&W) {
            if W.contains(&w) {
                neighbors.insert(w);
            }
        }
        neighbors
    }

    /// Returns the private neighborhood of v in respect to the set X
    pub fn privateNeighbors(&self, v: u32, X: &HashSet<u32>) -> HashSet<u32> {
        let mut Y = X.clone();
        Y.remove(&v);
        let mut N = HashSet::new();
        for (w, _) in &self.adj {
            let mut g = HashSet::new();
            g.insert(w.clone());
            let l: HashSet<u32> = Y.union(&self.neighbors_set(&g)).cloned().collect();
            let n: usize = l.len();
            if Y.contains(&w) == true && (w == &v || self.neighbors(v).contains(&w)) && n == 0 {
                N.insert(w.clone());
            }
        }
        N
    }

    /// Returns the total neighborhood of the graph
    pub fn total_neighborhood(&self, W: &HashSet<u32>) -> HashSet<u32> {
        let mut neighbors: HashSet<u32> = HashSet::new();
        for w in W {
            neighbors = neighbors.union(&self.neighbors(*w)).cloned().collect();
        }
        neighbors
    }

    /// Returns the set of the edges which are completely in W
    pub fn Delta(&self,  W: &HashSet<u32>) -> HashSet<(u32,u32)> {
        let mut F: HashSet<(u32, u32)> = HashSet::new();
        for v in W {
            for w in self.adj.get(&v).unwrap() {
                if W.contains(&w) && v < w {
                    F.insert((*v,*w));
                }
//                else if W.contains(&w) && v == w && withloops {
                    // TODO::Add functionality for loops
//                }
            }
        }
        F
    }

    /// Returns the set of the edges which leave W
    pub fn delta(&self,  W: &HashSet<u32>) -> HashSet<(u32,u32)> {
        let mut F: HashSet<(u32, u32)> = HashSet::new();
        for v in W {
            for w in self.adj.get(&v).unwrap() {
                if !W.contains(&w) {
                    F.insert((*v,*w));
                }
            }
        }
        F
    }

    /// Test if the vertices u and v are adjacent
    pub fn is_adjacent(&self, u: u32, v: u32) -> bool {
        self.hasEdge(u, v)
    }

    /// Tests if the vertex subset W is a clique in the graph
    pub fn is_clique(&self,  W: &HashSet<u32>) -> bool {
        let n: usize = W.len() - 1;
        for u in W {
            let neighbors: HashSet<_> = self.neighbors(*u);
            let intersect: HashSet<_> = neighbors.intersection(W).collect();
            if intersect.len() != n {
                return false
            }
        }
        true
    }

    /// Calculate the Cartesian product of self and H
    pub fn product(&self, H: &SGraph) -> SGraph {
        let mut G = SGraph::new();
        let mut vertices: Vec<(u32, u32)> = Vec::new();
        let mut i = 1;
        for (u,v) in iproduct!(self.getVertices().iter(),H.getVertices().iter()) {
            vertices.push((*u,*v));
            G.insertVertex(i);
            i = i + 1;
        }
        let mut i = 1;
        let mut j;
        for &(u,v) in &vertices {
            j = 1;
            for &(x,y) in &vertices {
                if u == x && H.is_adjacent(v,y) || v == y && self.is_adjacent(u,x) {
                    G.insertEdge(i, j);
                }
                j = j + 1;
            }
            i = i + 1;
        }
        G.graphtype = consts::graphType::CARTESIAN;
        G
    }

    /// Calculates the tensor product of self and H
    pub fn tensorProduct(&self, H: &SGraph) -> SGraph {
        let mut G = SGraph::new();
        let mut vertices: Vec<(u32, u32)> = Vec::new();
        let mut i = 1;
        for (u,v) in iproduct!(self.getVertices().iter(),H.getVertices().iter()) {
            vertices.push((*u,*v));
            G.insertVertex(i);
            i = i + 1;
        }
        let mut i = 1;
        let mut j;
        for &(u,v) in &vertices {
            j = 1;
            for &(x,y) in &vertices {
                if H.is_adjacent(v,y) && self.is_adjacent(u,x) {
                    G.insertEdge(i, j);
                }
                j = j + 1;
            }
            i = i + 1;
        }
        G.graphtype = consts::graphType::TENSOR;
        G
    }

    /// Calculate the lexicographical product of self and H
    pub fn lexicographicalProduct(&self, H: &SGraph) -> SGraph {
        let mut G = SGraph::new();
        let mut vertices: Vec<(u32, u32)> = Vec::new();
        let mut i = 1;
        for (u,v) in iproduct!(self.getVertices().iter(),H.getVertices().iter()) {
            vertices.push((*u,*v));
            G.insertVertex(i);
            i = i + 1;
        }
        let mut i = 1;
        let mut j;
        for &(u,v) in &vertices {
            j = 1;
            for &(x,y) in &vertices {
                if u == x && H.is_adjacent(v,y) || self.is_adjacent(u,x) {
                    G.insertEdge(i, j);
                }
                j = j + 1;
            }
            i = i + 1;
        }
        G.graphtype = consts::graphType::LEXI;
        G
    }

    /// Calculate the strong product of self and H
    pub fn strongProduct(&self, H: &SGraph) -> SGraph {
        let mut G = SGraph::new();
        let mut vertices: Vec<(u32, u32)> = Vec::new();
        let mut i = 1;
        for (u,v) in iproduct!(self.getVertices().iter(),H.getVertices().iter()) {
            vertices.push((*u,*v));
            G.insertVertex(i);
            i = i + 1;
        }
        let mut i = 1;
        let mut j;
        for &(u,v) in &vertices {
            j = 1;
            for &(x,y) in &vertices {
                if u == x && H.is_adjacent(v,y) || v == y && self.is_adjacent(u,x) || H.is_adjacent(v,y) && self.is_adjacent(u,x) {
                    G.insertEdge(i, j);
                }
                j = j + 1;
            }
            i = i + 1;
        }
        G.graphtype = consts::graphType::STRONG;
        G
    }

    /// Generates the join of self and H
    pub fn join(&self, H: &SGraph) -> SGraph {
        let mut G = self.copy();
        let n = self.order();

        // Add the vertices and edges of the second graph
        for (v, _) in &H.adj {
            G.insertVertex(v + n as u32);
        }
        for e in H.getEdgeSet() {
            G.insertEdge(e.0 + n as u32, e.1 + n as u32);
        }

        // Add the edges between the two parts of the join
        let m = G.order();
        for i in 1..n+1 {
            for j in n+1..m+1 {
                G.insertEdge(i as u32, j as u32);
            }
        }
        G
    }

    pub fn copy(&self) -> SGraph {
        let mut G = SGraph::new();
        G.insertVertexSet(&self.getV());
        G.insertEdgeSet(&self.getEdgeSet());
        G
    }
}


/// Generates the diamond graph
pub fn diamant() -> SGraph {
    let mut graph: SGraph = SGraph::new();
    graph.insertVertex(1);
    graph.insertVertex(2);
    graph.insertVertex(3);
    graph.insertVertex(4);
    graph.insertEdge(1,2);
    graph.insertEdge(1,3);
    graph.insertEdge(2,3);
    graph.insertEdge(2,4);
    graph.insertEdge(3,4);

    graph
}

/// Generates a path with n vertices
pub fn path(n: u32) -> SGraph {
    let mut graph: SGraph = SGraph::new();
    let V: Vec<u32> = (1..n+1).collect();
    let V: HashSet<u32> = V.into_iter().collect();
    graph.insertVertexSet(&V);
    for u in 1..n {
        graph.insertEdge(u,u+1);
    }
    graph.graphtype = consts::graphType::PATH;
    graph
}

/// Generates a cycle with n vertices
pub fn cycle(n: u32) -> SGraph {
    let mut graph: SGraph = SGraph::new();
    let V: Vec<u32> = (1..n+1).collect();
    let V: HashSet<u32> = V.into_iter().collect();
    graph.insertVertexSet(&V);
    for u in 1..n {
        graph.insertEdge(u,u+1);
    }
    graph.insertEdge(1,n);
    graph.graphtype = consts::graphType::CYCLE;
    graph
}

/// Generates a anti-cycle with n vertices
pub fn anticycle(n: u32) -> SGraph {
    if n == 3 {
        let mut graph: SGraph = SGraph::new();
        graph.insertVertex(1);
        graph.insertVertex(2);
        graph.insertVertex(3);
        return graph
    }
    let mut graph: SGraph = SGraph::new();
    let V: Vec<u32> = (1..n+1).collect();
    let V: HashSet<u32> = V.into_iter().collect();
    graph.insertVertexSet(&V);
    for v in 2..n {
        for u in 1..n+1 {
            graph.insertEdge(u,v);
        }
    }
    graph.graphtype = consts::graphType::ANTICYCLE;
    graph
}

/// Generates a complete graph with n vertices
pub fn completeGraph(n: u32) -> SGraph {
    let mut graph: SGraph = SGraph::new();
    let V: Vec<u32> = (1..n+1).collect();
    let V: HashSet<u32> = V.into_iter().collect();
    graph.insertVertexSet(&V);
    for u in 1..n {
        for v in u+1..n+1 {
            graph.insertEdge(u,v);
        }
    }
    graph.graphtype = consts::graphType::COMPLETE;
    graph
}

/// Generates a complete bipartite graph with n vertices
pub fn completeBipartite(p: u32, q: u32) -> SGraph {
    let mut graph: SGraph = SGraph::new();
    let V: Vec<u32> = (1..p+q+1).collect();
    let V: HashSet<u32> = V.into_iter().collect();
    graph.insertVertexSet(&V);
    for u in 1..p+1 {
        for v in p+1..p+q+1 {
            graph.insertEdge(u,v);
        }
    }
    graph.graphtype = consts::graphType::COMPBIPARTITE;
    graph
}

/// Generates a star graph with n vertices
pub fn star(n: u32) -> SGraph {
    let mut graph: SGraph = SGraph::new();
    let V: Vec<u32> = (1..n+1).collect();
    let V: HashSet<u32> = V.into_iter().collect();
    graph.insertVertexSet(&V);
    for u in 2..n+1 {
        graph.insertEdge(1,u);
    }
    graph.graphtype = consts::graphType::STAR;
    graph
}
