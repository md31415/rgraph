# rgraph

rgraph is a library for an undirected graph written in Rust. At the moment 
only some basic function to manipulate the graph are available but the 
library will be extended in the next time. The goal is to have a compatible 
library for usage in PyGraphEdit (gitlab.com/md31415/PyGraphEdit).
